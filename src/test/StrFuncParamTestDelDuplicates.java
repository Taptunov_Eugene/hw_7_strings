import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;


import java.util.Arrays;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class StrFuncParamTestDelDuplicates {
    private String valueA;
    private String expected;

    public StrFuncParamTestDelDuplicates(String valueA, String expected) {
        this.valueA = valueA;
        this.expected = expected;
    }

    @Parameterized.Parameters()
    public static Iterable<Object[]> Test() {
        return Arrays.asList(new Object[][]{
                {"maaaakkee 111124900 feelings",
                        "[m, a, k, e,  , 1, 2, 4, 9, 0, f, l, i, n, g, s]"},
                {"111111", "[1]"},
        });
    }

    @Test
    public void deleteSymbolDuplicates() {
        assertEquals(expected, new StringWorkingFunction().
                deleteSymbolDuplicates(valueA));
    }


}
