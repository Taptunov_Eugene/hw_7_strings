import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;


import java.util.Arrays;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class StrFuncParamTestReplaceTo$ {
    private String[] valueA;
    private int valueB;
    private String[] expected;


    public StrFuncParamTestReplaceTo$(String[] valueA, int valueB, String[] expected) {
        this.valueA = valueA;
        this.valueB = valueB;
        this.expected = expected;
    }

    @Parameterized.Parameters()
    public static Iterable<Object[]> Test() {
        return Arrays.asList(new Object[][]{
                {new String[]{"Replace", "last", "three", "symbols"}, 7
                        , new String[]{"Repl$$$", "last", "three", "symb$$$"}},
                {new String[]{"Replace", "last", "three", "symbols"}, 4
                        , new String[]{"Replace", "l$$$", "three", "symbols"}},
        });
    }

    @Test
    public void replaceThreeLastWordSymbolsTo$() {
        assertEquals(expected, new StringWorkingFunction().
                replaceThreeLastWordSymbolsTo$(valueA, valueB));
    }
}
