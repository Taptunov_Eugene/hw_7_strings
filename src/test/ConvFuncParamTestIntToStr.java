import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;

@RunWith(Parameterized.class)
public class ConvFuncParamTestIntToStr {

    private int valueA;
    private String expected;

    public ConvFuncParamTestIntToStr(int valueA, String expected) {
        this.valueA = valueA;
        this.expected = expected;
    }

    @Parameterized.Parameters()
    public static Iterable<Object[]> Test() {
        return Arrays.asList(new Object[][]{
                {5, "5"},
                {6, "6"}
        });
    }

    @Test
    public void intToStringTest() {
        new ConversionFunction();
        assertEquals(expected,  ConversionFunction.intNumberToString(valueA));
    }
}