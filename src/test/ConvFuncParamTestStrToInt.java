import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;

@RunWith(Parameterized.class)
public class ConvFuncParamTestStrToInt {

    private String  valueA;
    private int expected;

    public ConvFuncParamTestStrToInt(String  valueA, int expected) {
        this.valueA = valueA;
        this.expected = expected;
    }

    @Parameterized.Parameters()
    public static Iterable<Object[]> Test() {
        return Arrays.asList(new Object[][]{
                {"5", 5},
                {"6", 6}
        });
    }

    @Test
    public void intToStringTest() {
        assertEquals(expected,  ConversionFunction.stringToIntNumber(valueA));
    }
}