import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;

@RunWith(Parameterized.class)
public class ConvFuncParamTestStrToDouble {

    private String valueA;
    private Double expected;

    public ConvFuncParamTestStrToDouble(String  valueA, Double expected) {
        this.valueA = valueA;
        this.expected = expected;
    }

    @Parameterized.Parameters()
    public static Iterable<Object[]> Test() {
        return Arrays.asList(new Object[][]{
                {"5.45", 5.45},
                {"6.456", 6.456}
        });
    }

    @Test
    public void stringToDoubleNumber() {
        assertEquals(java.util.Optional.ofNullable(expected),
                java.util.Optional.ofNullable(ConversionFunction.stringToDoubleNumber(valueA)));
    }
}