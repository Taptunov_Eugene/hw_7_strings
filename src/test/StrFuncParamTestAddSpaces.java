import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;


import java.util.Arrays;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class StrFuncParamTestAddSpaces {
    private String valueA;
    private String expected;

    public StrFuncParamTestAddSpaces(String valueA, String expected) {
        this.valueA = valueA;
        this.expected = expected;
    }

    @Parameterized.Parameters()
    public static Iterable<Object[]> Test() {
        return Arrays.asList(new Object[][]{
                {"Replaces,each,substring", "Replaces, each, substring"},
                {"of;this?string that", "of; this? string that"},
        });
    }

    @Test
    public void addSpacesAfterPunctuationMarks() {
        assertEquals(expected, new StringWorkingFunction().addSpacesAfterPunctuationMarks(valueA));
    }
}
