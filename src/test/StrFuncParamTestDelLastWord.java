import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;


import java.util.Arrays;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class StrFuncParamTestDelLastWord {
    private String valueA;

    private String expected;

    public StrFuncParamTestDelLastWord(String valueA, String expected) {
        this.valueA = valueA;

        this.expected = expected;
    }

    @Parameterized.Parameters()
    public static Iterable<Object[]> Test() {
        return Arrays.asList(new Object[][]{
                {"String without last word is",
                        "String without last word is: " +
                                "String without last word"},
                {"String without last word",
                        "String without last word is: String without last"},
        });
    }

    @Test
    public void removeLastWordInString() {
        assertEquals(expected, new StringWorkingFunction().
                removeLastWordInString(valueA));
    }


}
