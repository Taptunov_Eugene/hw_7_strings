import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;


import java.util.Arrays;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class StrFuncParamTestShortWord {
    private String valueA;
    private String expected;

    public StrFuncParamTestShortWord(String valueA, String expected) {
        this.valueA = valueA;
        this.expected = expected;
    }

    @Parameterized.Parameters()
    public static Iterable<Object[]> Test() {
        return Arrays.asList(new Object[][]{
                {"The shortest word in the sentence",
                        "The shortest word in the sentence: in"},
                {"Input word arrays", "The shortest word in the " +
                        "sentence: word"}
        });
    }

    @Test
    public void shortestWordInSentence() {
        assertEquals(expected, new StringWorkingFunction().
                shortestWordInSentence(valueA));
    }
}
