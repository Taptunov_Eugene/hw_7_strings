import org.junit.Assert;

import static org.junit.jupiter.api.Assertions.*;

class ConversionFunctionTest {

    @org.junit.jupiter.api.Test
    void intNumberToString() {
        String actual = "5";
        String expected = ConversionFunction.intNumberToString(5);
        Assert.assertEquals(actual, expected);

    }

    @org.junit.jupiter.api.Test
    void doubleNumberToString() {
        String actual = "5.32445";
        String expected = ConversionFunction.doubleNumberToString(5.32445);
        Assert.assertEquals(actual, expected);
    }

    @org.junit.jupiter.api.Test
    void stringToIntNumber() {
        int actual = 5;
        int expected = ConversionFunction.stringToIntNumber("5");
        Assert.assertEquals(actual, expected);
    }

    @org.junit.jupiter.api.Test
    void stringToDoubleNumber() {
        double actual = 5.2435;
        double expected = ConversionFunction.stringToDoubleNumber("5.2435");
        Assert.assertEquals(actual, expected, 0.4);
    }
}