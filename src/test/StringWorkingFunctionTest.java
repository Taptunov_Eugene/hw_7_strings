import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.util.Scanner;

class StringWorkingFunctionTest {

    @Test
    void shortestWordInSentence() {
        String actual = "The shortest word in the sentence: is";
        String expected = StringWorkingFunction.
                shortestWordInSentence("Java is the best!");
        Assert.assertEquals(actual, expected);
    }

    @Test
    void replaceThreeLastWordSymbolsTo$() {
        String[] actual = {"Replace", "last", "three", "symbols", "in", "words",
                "with", "specified", "length", "to"};

        String[] expected = StringWorkingFunction.
                replaceThreeLastWordSymbolsTo$(actual, 7);

        Assert.assertEquals(actual, expected);
    }

    @Test
    void addSpacesAfterPunctuationMarks() {
        String actual = "Replaces, each, substring, of; this? string " +
                "that matches the given. regular expression " +
                "with# the@ given replacement.";
        String expected = StringWorkingFunction.
                addSpacesAfterPunctuationMarks("Replaces,each,substring, " +
                "of;this?string that matches the given.regular expression " +
                        "with#the@given replacement.");
        Assert.assertEquals(actual, expected);
    }

    @Test
    void deleteSymbolDuplicates() {
        String actual = "[m, a, k, e,  , 1, 2, 4, 9, 0, f, l, i, n, g, s]";
        String expected = StringWorkingFunction.
                deleteSymbolDuplicates("maaaakkee 111124900 feelings");
        Assert.assertEquals(actual, expected);
    }

    @Test
    void numberOfWordsInString() {

    }

    @Test
    void removeCharsFromSpecifiedPositionAndLength() {
        String actual = "Java ie best!";
        String expected = StringWorkingFunction.
                removeCharsFromSpecifiedPositionAndLength("Java is the " +
                        "best!", 6, 4);
        Assert.assertEquals(actual, expected);
    }

    @Test
    void stringReverse() {
        String actual = "Reversed string is: !tseb eht si avaJ";
        String expected = StringWorkingFunction.
                stringReverse("Java is the best!");
        Assert.assertEquals(actual, expected);
    }

    @Test
    void removeLastWordInString() {
        String actual = "String without last word is: Java is the";
        String expected = StringWorkingFunction.
                removeLastWordInString("Java is the best!");
        Assert.assertEquals(actual, expected);
    }
}