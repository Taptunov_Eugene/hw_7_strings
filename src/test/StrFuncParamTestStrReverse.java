import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;


import java.util.Arrays;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class StrFuncParamTestStrReverse {
    private String valueA;

    private String expected;

    public StrFuncParamTestStrReverse(String valueA, String expected) {
        this.valueA = valueA;
        this.expected = expected;
    }

    @Parameterized.Parameters()
    public static Iterable<Object[]> Test() {
        return Arrays.asList(new Object[][]{
                {"Input string is", "Reversed string is: si gnirts tupnI"},
                {"Reversed string is",
                        "Reversed string is: si gnirts desreveR"},
        });
    }

    @Test
    public void stringReverse() {
        assertEquals(expected, new StringWorkingFunction().
                stringReverse(valueA));
    }
}
