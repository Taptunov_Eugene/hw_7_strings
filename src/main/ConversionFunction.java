package main;

/**
 * @version     1.1 august 2020
 * @author      Eugene
 */
public class ConversionFunction {

    public static String intNumberToString(int number) {
        try {
            return Integer.toString(number);
        } catch (NumberFormatException exception) {
            System.out.println("Wrong String format");
        }
        return "";
    }

    public static String doubleNumberToString(double number) {
        try {
            return Double.toString(number);
        } catch (NumberFormatException exception) {
            System.out.println("Wrong String format");
        }
        return "";
    }

    public static int stringToIntNumber(String number) {
        try {
            return Integer.valueOf(number);
        } catch (NumberFormatException exception) {
            System.out.println("Wrong String format");
        }
        return 0;
    }

    public static double stringToDoubleNumber(String number) {
        try {
            return Double.valueOf(number);
        } catch (NumberFormatException exception) {
            System.out.println("Wrong String format");
        }
        return 0;    }

    public static void main(String[] args) {
        System.out.println(intNumberToString(25));
        System.out.println(doubleNumberToString(243.657));
        System.out.println(stringToIntNumber("567"));
        System.out.println(stringToDoubleNumber("32.8722341"));
    }
}
