package main;

/**
 * @version     1.1 august 2020
 * @author      Eugene
 */
public class OutputToOneLineCharacters {

    public static char englishUppercase() {
        for (char i = 65; i < 91; i++)
            System.out.print(i + " ");
        return 0;
    }

    public static char englishLowercase() {
        for (char i = 97; i < 123; i++)
            System.out.print(i + " ");
        return 0;
    }

    public static char russianLowercase() {
        for (char i = 1072; i <= 1103; i++)
            System.out.print(i + " ");
        return 0;
    }

    public static char numbersOneToNine() {
        for (char i = 48; i <= 57; i++)
            System.out.print(i + " ");
        return 0;
    }

    public static char printableRangeOfASCII() {
        for (char i = 32; i <= 126; i++)
            System.out.print(i + " ");
        return 0;
    }

    public static void main(String[] args) {
        System.out.println(englishUppercase());
        System.out.println(englishLowercase());
        System.out.println(russianLowercase());
        System.out.println(numbersOneToNine());
        System.out.println(printableRangeOfASCII());
    }
}
