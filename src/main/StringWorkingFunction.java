package main;

import java.util.*;

/**
 * @version     1.1 august 2020
 * @author      Eugene
 */
public class StringWorkingFunction {

    public static String shortestWordInSentence(String sentence) {
        List<String> words;

        words = Arrays.asList(sentence.split(" "));
        String shortestWord = words.stream().min(Comparator.comparingInt(String::length)).get();

        return "The shortest word in the sentence: " + shortestWord;
    }

    public static String[] replaceThreeLastWordSymbolsTo$(String[] words, int wordLength) {
        System.out.println("Input words array is: " + Arrays.toString(words));
        for (int i = 0; i < words.length; i++) {
            if (words[i].length() == wordLength) {
                words[i] = words[i].replace(words[i].substring(words[i].length() - 3),
                        "$$$");
            }
        }
        return words;
    }

    public static String addSpacesAfterPunctuationMarks(String string) {
        string = string.replaceAll("(?<=\\p{Punct})(?=\\w)", " ");
        return string;
    }

    public static String deleteSymbolDuplicates(String inputString) {
        System.out.println("Input String is: " + inputString);
        StringBuilder stringBuilder = new StringBuilder();
        Set<Character> linkedHashSet = new LinkedHashSet<>();

        for (int i = 0; i < inputString.length(); i++) {
            linkedHashSet.add(inputString.charAt(i));
        }
        for (Character symbol : linkedHashSet) {
            stringBuilder.append(symbol);
        }

        return linkedHashSet.toString();
    }

    public static int numberOfWordsInString() {
        Scanner string = new Scanner(System.in);
        System.out.println("Enter words on one line separated by a space");
        String input = string.nextLine();
        int count = 0;

        if (input.length() != 0) {
            count++;
            for (int i = 0; i < input.length(); i++) {
                if (input.charAt(i) == ' ') {
                    count++;
                }
            }
        }
        return count;
    }

    public static String removeChartsFromSpecifiedPositionAndLength(String string,
                                                                    int position, int length) {
        return string.substring(0, position) + string.substring(position + length);
    }

    public static String stringReverse(String inputString) {
        System.out.println("Input string is: " + inputString);
        String reversedString = new StringBuffer(inputString).reverse().toString();
        return "Reversed string is: " + reversedString;
    }

    public static String removeLastWordInString(String inputString) {
        System.out.println("Input string is: " + inputString);
        String outputString = inputString.substring(0, inputString.lastIndexOf(" "));
        return "String without last word is: " + outputString;
    }

    public static void main(String[] args) {

        System.out.println(shortestWordInSentence("Java is the best!"));
        System.out.println();

        System.out.println("Output array of words is: "
                + Arrays.toString(replaceThreeLastWordSymbolsTo$(new String[]{"Replace", "last",
                "three", "symbols", "in", "words", "with", "specified", "length", "to"}, 7)));
        System.out.println();

        System.out.println(addSpacesAfterPunctuationMarks("Replaces,each,substring, " +
                "of;this?string that matches the given.regular expression with#the@given replacement."));
        System.out.println();

        System.out.println("Output string without symbol duplicates"
                + deleteSymbolDuplicates("maaaakkee 111124900 feelings"));
        System.out.println();

        System.out.println("You entered "
                + numberOfWordsInString() + " words");
        System.out.println();

        System.out.println("The string without removed part from the specified position " +
                "and specified length is: " +
                removeChartsFromSpecifiedPositionAndLength("Java is the best!",
                        6, 4));
        System.out.println();

        System.out.println(stringReverse("Java is the best!"));
        System.out.println();

        System.out.println(removeLastWordInString("Java is the best!"));
    }
}
